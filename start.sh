# start system script

# build lib
docker-compose -f atendimento-lib/docker-compose.yml up

# copy lib to all systems
mkdir -p ./atendimento-gateway/lib
mkdir -p ./atendimento-agendamento-backend/lib
mkdir -p ./atendimento-paciente-backend/lib
cp ./atendimento-lib/target/atendimento-lib-0.0.1-SNAPSHOT.jar ./atendimento-gateway/lib/atendimento-lib-0.0.1-SNAPSHOT.jar
cp ./atendimento-lib/target/atendimento-lib-0.0.1-SNAPSHOT.jar ./atendimento-agendamento-backend/lib/atendimento-lib-0.0.1-SNAPSHOT.jar
cp ./atendimento-lib/target/atendimento-lib-0.0.1-SNAPSHOT.jar ./atendimento-paciente-backend/lib/atendimento-lib-0.0.1-SNAPSHOT.jar

# run the system
docker-compose up