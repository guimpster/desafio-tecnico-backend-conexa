-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `agendamento`;
CREATE TABLE `agendamento` (
  `id_agendamento` int(11) NOT NULL AUTO_INCREMENT,
  `id_medico` int(11) NOT NULL,
  `id_paciente` int(11) NOT NULL,
  `data_hora_atendimento` datetime NOT NULL,
  PRIMARY KEY (`id_agendamento`),
  KEY `id_medico` (`id_medico`),
  KEY `id_paciente` (`id_paciente`),
  CONSTRAINT `agendamento_ibfk_1` FOREIGN KEY (`id_medico`) REFERENCES `medico` (`id_medico`),
  CONSTRAINT `agendamento_ibfk_2` FOREIGN KEY (`id_paciente`) REFERENCES `paciente` (`id_paciente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `agendamento` (`id_agendamento`, `id_medico`, `id_paciente`, `data_hora_atendimento`) VALUES
(5,	1,	1,	'2020-08-03 12:00:00'),
(6,	1,	1,	'2020-08-03 12:00:00'),
(7,	1,	1,	'2020-08-03 12:00:00'),
(8,	1,	1,	'2020-08-03 12:00:00');

DROP TABLE IF EXISTS `medico`;
CREATE TABLE `medico` (
  `id_medico` int(11) NOT NULL AUTO_INCREMENT,
  `medico` varchar(255) NOT NULL,
  `especialidade` varchar(255) NOT NULL,
  `usuario` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `token` blob,
  PRIMARY KEY (`id_medico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `medico` (`id_medico`, `medico`, `especialidade`, `usuario`, `senha`, `token`) VALUES
(1,	'Wagner',	'Clínico Geral',	'wagner',	'abc123',	NULL),
(2,	'Andréia',	'Gastroenterologista',	'andreia',	'abc123',	'');

DROP TABLE IF EXISTS `paciente`;
CREATE TABLE `paciente` (
  `id_paciente` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `cpf` varchar(11) NOT NULL,
  `idade` int(3) NOT NULL,
  `telefone` varchar(19) NOT NULL,
  PRIMARY KEY (`id_paciente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `paciente` (`id_paciente`, `nome`, `cpf`, `idade`, `telefone`) VALUES
(1,	'João Silva',	'80768275075',	21,	'+55 (11) 94543-0293'),
(2,	'João Silva',	'56595451220',	21,	'+55 (11) 94543-0293'),
(4,	'João Silva',	'20207762007',	21,	'+55 (11) 94543-0293'),
(5,	'Santanna',	'58885354351',	33,	'+55 (11) 94543-0293');


DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `token` blob NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `usuario` (`id_usuario`, `usuario`, `senha`, `token`) VALUES
(1,	'wagner',	'abc123',	''),
(2,	'andreia',	'abc123',	'');

-- 2020-11-15 21:02:02