package com.conexa.atendimento.lib.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.conexa.atendimento.lib.domain.Medico;

@Repository
public interface MedicoRepository extends CrudRepository<Medico, Long> {
	public Medico findByUsuarioAndSenha(String usuario, String senha);
	public Medico findByToken(String token);
}
