package com.conexa.atendimento.lib.constants;

import static com.conexa.atendimento.lib.util.StringUtils.getMensagemPadrao;

public class AtendimentoConstants {
	public static final String NOME_PROJETO = "Atendimento";
	
	public static final String RESPONSE_200 = getMensagemPadrao("response.code200");
	public static final String RESPONSE_201 = getMensagemPadrao("response.code201");
	public static final String RESPONSE_204 = getMensagemPadrao("response.code204");

	public static final String RESPONSE_304 = getMensagemPadrao("response.code304");

	public static final String RESPONSE_400 = getMensagemPadrao("response.code400");
	public static final String RESPONSE_401 = getMensagemPadrao("response.code401");
	public static final String RESPONSE_404 = getMensagemPadrao("response.code404");

	public static final String RESPONSE_500 = getMensagemPadrao("response.code500");
	
	public static final String DATE_FORMAT = "uuuu-MM-dd HH:mm:ss";
}
