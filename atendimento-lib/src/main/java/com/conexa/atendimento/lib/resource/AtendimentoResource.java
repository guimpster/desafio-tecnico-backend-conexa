package com.conexa.atendimento.lib.resource;


import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.conexa.atendimento.lib.dto.AtendimentoResponse;
import com.conexa.atendimento.lib.enumerate.AtendimentoHttpEnum;

public interface AtendimentoResource {

	default <T extends Object> ResponseEntity<AtendimentoResponse<T>> retornarResponse(final AtendimentoHttpEnum httpEnum,
			final T response) {
		return ResponseEntity.status(httpEnum.getStatus()).body(new AtendimentoResponse<>(httpEnum, response));
	}

	/*
	 * HTTP 200
	 */
	default <T extends Object> ResponseEntity<AtendimentoResponse<T>> retornarSucesso(final T response) {
		return response != null ? retornarResponse(AtendimentoHttpEnum.HTTP_200, response) : retornarSemConteudo();
	}

	/*
	 * HTTP 200
	 */
	default <T extends Object> ResponseEntity<AtendimentoResponse<List<T>>> retornarSucesso(final List<T> response) {
		return !response.isEmpty() ? retornarResponse(AtendimentoHttpEnum.HTTP_200, response) : retornarSemConteudo();
	}

	/**
	 * <p>
	 * Através de um {@link Optional} verifica se há um retorno para a chamada, em
	 * caso positivo, retorna http 200 com o recurso retornado, caso contrário,
	 * retorna http 204 com uma mensagem padrão de recurso não encontrado.
	 *
	 * @param responseOpt
	 * @return
	 */
	default <T extends Object> ResponseEntity<AtendimentoResponse<T>> retornarSucesso(final Optional<T> responseOpt) {
		return responseOpt.isPresent() ? retornarResponse(AtendimentoHttpEnum.HTTP_200, responseOpt.get()) : retornarSemConteudo();
	}

	/**
	 * Este método deve ser utilizado apenas pelos serviços de health.
	 *
	 * @return
	 */
	default <T extends Object> ResponseEntity<AtendimentoResponse<T>> retornarSucesso() {
		return retornarResponse(AtendimentoHttpEnum.HTTP_200, null);
	}

	/*
	 * HTTP 201
	 */
	default <T extends Object> ResponseEntity<AtendimentoResponse<T>> retornarCriado(final T response) {
		return retornarResponse(AtendimentoHttpEnum.HTTP_201, response);
	}

	default <T extends Object> ResponseEntity<AtendimentoResponse<T>> retornarCriado() {
		return retornarResponse(AtendimentoHttpEnum.HTTP_201, null);
	}

	/**
	 * @deprecated - Utilizar o método {@link UltronResource#retornarSemConteudo()}
	 * @return
	 */
	@Deprecated
	default <T extends Object> ResponseEntity<AtendimentoResponse<T>> retornarExcluido() {
		return retornarSemConteudo();
	}

	/*
	 * HTTP 204
	 */
	default <T extends Object> ResponseEntity<AtendimentoResponse<T>> retornarSemConteudo() {
		return retornarResponse(AtendimentoHttpEnum.HTTP_204, null);
	}

	/*
	 * HTTP 304
	 */
	default <T extends Object> ResponseEntity<AtendimentoResponse<T>> retornarNaoAlterado() {
		return retornarResponse(AtendimentoHttpEnum.HTTP_304, null);
	}

	/**
	 * @deprecated - Utilizar o método {@link UltronResource#retornarSemConteudo()}
	 * @return
	 */
	@Deprecated
	default <T extends Object> ResponseEntity<AtendimentoResponse<T>> retornarNaoEncontrado() {
		return retornarSemConteudo();
	}
}