package com.conexa.atendimento.lib.enumerate;

import org.springframework.http.HttpStatus;

import com.conexa.atendimento.lib.constants.AtendimentoConstants;
import com.conexa.atendimento.lib.util.StringUtils;

public enum AtendimentoHttpEnum {

	HTTP_200(HttpStatus.OK, AtendimentoConstants.RESPONSE_200),
	HTTP_201(HttpStatus.CREATED, AtendimentoConstants.RESPONSE_201),
	HTTP_204(HttpStatus.OK, AtendimentoConstants.RESPONSE_204),

	HTTP_304(HttpStatus.NOT_MODIFIED, AtendimentoConstants.RESPONSE_304),

	HTTP_400(HttpStatus.BAD_REQUEST, AtendimentoConstants.RESPONSE_400),
	HTTP_401(HttpStatus.UNAUTHORIZED, AtendimentoConstants.RESPONSE_401),
	HTTP_404(HttpStatus.NOT_FOUND, AtendimentoConstants.RESPONSE_404),
	HTTP_500(HttpStatus.INTERNAL_SERVER_ERROR, AtendimentoConstants.RESPONSE_500);

	private HttpStatus status;
	private String mensagem;

	private AtendimentoHttpEnum(final HttpStatus status, final String mensagem) {
		this.status = status;
		this.mensagem = mensagem;
	}

	public static AtendimentoHttpEnum fromValue(final HttpStatus status) {

		for (final AtendimentoHttpEnum httpEnum : values()) {
			if (httpEnum.getStatus().equals(status)) {
				return httpEnum;
			}
		}

		throw new IllegalArgumentException(
				StringUtils.getMensagemPadrao("exception.enum.codigoInvalido", status.value()));
	}

	public HttpStatus getStatus() {
		return status;
	}

	public String getMensagem() {
		return mensagem;
	}

	public boolean is2xxSuccessful() {
		return status.is2xxSuccessful();
	}

	public boolean is4xxClientError() {
		return status.is4xxClientError();
	}

	public boolean is5xxServerError() {
		return status.is5xxServerError();
	}
}