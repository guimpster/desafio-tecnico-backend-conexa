package com.conexa.atendimento.lib.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginMedicoDto extends AtendimentoDto {

	private static final long serialVersionUID = 1404633711362526354L;
	
	private String token;
	
	private String medico;
	
	private String especialidade;
	
	private List<AgendamentoDto> agendamentoHoje;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getMedico() {
		return medico;
	}

	public void setMedico(String medico) {
		this.medico = medico;
	}

	public String getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}

	public List<AgendamentoDto> getAgendamentoHoje() {
		return agendamentoHoje;
	}

	public void setAgendamentoHoje(List<AgendamentoDto> agendamentoHoje) {
		this.agendamentoHoje = agendamentoHoje;
	}

}
