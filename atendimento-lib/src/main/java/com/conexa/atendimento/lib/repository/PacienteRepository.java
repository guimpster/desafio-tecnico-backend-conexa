package com.conexa.atendimento.lib.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.conexa.atendimento.lib.domain.Paciente;

@Repository
public interface PacienteRepository extends CrudRepository<Paciente, Long> {
	public List<Paciente> findByCpf(String cpf);
}
