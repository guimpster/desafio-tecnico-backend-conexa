package com.conexa.atendimento.lib.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PacienteDto extends AtendimentoDto {
	private static final long serialVersionUID = -8606746831252669246L;
	
    private Long id; 
	
	private String nome;
	
	private String cpf;
	
	private Integer idade;
	
	private String telefone;

	public Long getId() {
		return id;
	}

	public void setId(Long idPaciente) {
		this.id = idPaciente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
}
