package com.conexa.atendimento.lib.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AgendamentoDto extends AtendimentoDto {

	private static final long serialVersionUID = 4936156111471733561L;

	private Long idPaciente;
	
	private String dataHoraAtendimento;

	public Long getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Long idPaciente) {
		this.idPaciente = idPaciente;
	}

	public String getDataHoraAtendimento() {
		return dataHoraAtendimento;
	}

	public void setDataHoraAtendimento(String dataHoraAtendimento) {
		this.dataHoraAtendimento = dataHoraAtendimento;
	}
	
}
