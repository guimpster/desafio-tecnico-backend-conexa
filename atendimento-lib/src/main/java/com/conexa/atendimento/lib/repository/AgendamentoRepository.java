package com.conexa.atendimento.lib.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.conexa.atendimento.lib.domain.Agendamento;

@Repository
public interface AgendamentoRepository extends CrudRepository<Agendamento, Long> {
	public List<Agendamento> findByMedicoIdMedico(Long idMedico);
}
