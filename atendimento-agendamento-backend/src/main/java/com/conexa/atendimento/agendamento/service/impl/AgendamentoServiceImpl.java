package com.conexa.atendimento.agendamento.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.conexa.atendimento.agendamento.mapper.AgendamentoMapper;
import com.conexa.atendimento.agendamento.service.AgendamentoService;
import com.conexa.atendimento.lib.domain.Agendamento;
import com.conexa.atendimento.lib.dto.AgendamentoDto;
import com.conexa.atendimento.lib.repository.AgendamentoRepository;

@Service
public class AgendamentoServiceImpl implements AgendamentoService {
	
	@Autowired
	private AgendamentoRepository agendamentoRepository;
	
	@Autowired
	private AgendamentoMapper agendamentoMapper;

	public AgendamentoDto agendar(AgendamentoDto agendamentoDto, Long idMedico) {
		Agendamento agendamento = agendamentoMapper.mapearAgendamento(agendamentoDto, idMedico);
		Agendamento agendamentoDb = agendamentoRepository.save(agendamento);
		return agendamentoMapper.mapearAgendamentoDto(agendamentoDb);
	}

	public List<AgendamentoDto> listarAgendamentos(Long idMedico) {
		List<Agendamento> agendamentos = agendamentoRepository.findByMedicoIdMedico(idMedico);
		return agendamentoMapper.mapearAgendamentosDto(agendamentos);
	}
}
