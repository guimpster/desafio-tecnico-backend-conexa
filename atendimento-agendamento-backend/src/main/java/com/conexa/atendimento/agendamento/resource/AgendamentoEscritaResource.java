package com.conexa.atendimento.agendamento.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.conexa.atendimento.agendamento.service.AgendamentoService;
import com.conexa.atendimento.lib.dto.AgendamentoDto;
import com.conexa.atendimento.lib.enumerate.AtendimentoHttpEnum;
import com.conexa.atendimento.lib.resource.AtendimentoResource;

@RestController
@RequestMapping("/atendimento-agendamento")
public class AgendamentoEscritaResource implements AtendimentoResource {
	
	@Autowired
	private AgendamentoService agendamentoService;
	
	@PostMapping("/agendamento")
	public ResponseEntity<?> agendar(
			@RequestBody(required = true) AgendamentoDto agendamentoDto,
			@RequestHeader("idMedico") Long idMedico) {	
		try {
			AgendamentoDto agendamento = agendamentoService.agendar(agendamentoDto, idMedico);
			return retornarSucesso(agendamento);
		} catch (Exception e) {
			return retornarResponse(AtendimentoHttpEnum.HTTP_400, e.getMessage());
		}
	}
}
