package com.conexa.atendimento.agendamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.conexa.atendimento.lib.repository.AgendamentoRepository;

@SpringBootApplication
@EntityScan({ "com.conexa.atendimento.lib.domain" })
@EnableJpaRepositories(basePackageClasses = {AgendamentoRepository.class})
public class AtendimentoAgendamentoBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(AtendimentoAgendamentoBackendApplication.class, args);
	}

}
