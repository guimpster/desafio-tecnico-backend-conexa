package com.conexa.atendimento.agendamento.mapper;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.conexa.atendimento.lib.constants.AtendimentoConstants;
import com.conexa.atendimento.lib.domain.Agendamento;
import com.conexa.atendimento.lib.domain.Medico;
import com.conexa.atendimento.lib.domain.Paciente;
import com.conexa.atendimento.lib.dto.AgendamentoDto;

@Component
public class AgendamentoMapper {
	
	private static DateTimeFormatter formater = DateTimeFormatter.ofPattern(AtendimentoConstants.DATE_FORMAT);

	public Agendamento mapearAgendamento(AgendamentoDto agendamentoDto, Long idMedico) {
		Agendamento agendamento = new Agendamento();
		agendamento.setIdMedico(idMedico);
		agendamento.setIdPaciente(agendamentoDto.getIdPaciente());
		agendamento.setDataHoraAtendimento(LocalDateTime.parse(agendamentoDto.getDataHoraAtendimento(), formater));
		return agendamento;
	}

	public AgendamentoDto mapearAgendamentoDto(Agendamento agendamento) {
		AgendamentoDto agendamentoDto = new AgendamentoDto();
		agendamentoDto.setIdPaciente(agendamento.getIdPaciente());
		agendamentoDto.setDataHoraAtendimento(agendamento.getDataHoraAtendimento().format(formater));
		return agendamentoDto;
	}

	public List<AgendamentoDto> mapearAgendamentosDto(List<Agendamento> agendamentos) {
		if (agendamentos == null) return Collections.emptyList();
		return agendamentos.parallelStream().map(this::mapearAgendamentoDto).collect(Collectors.toList());
	}
}
