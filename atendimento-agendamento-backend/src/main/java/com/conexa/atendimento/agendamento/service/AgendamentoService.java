package com.conexa.atendimento.agendamento.service;

import java.util.List;

import com.conexa.atendimento.lib.dto.AgendamentoDto;

public interface AgendamentoService {
	public AgendamentoDto agendar(AgendamentoDto agendamentoDto, Long idMedico);

	public List<AgendamentoDto> listarAgendamentos(Long idMedico);
}
