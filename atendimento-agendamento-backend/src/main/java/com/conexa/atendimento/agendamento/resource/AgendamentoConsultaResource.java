package com.conexa.atendimento.agendamento.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.conexa.atendimento.agendamento.service.AgendamentoService;
import com.conexa.atendimento.lib.dto.AgendamentoDto;
import com.conexa.atendimento.lib.enumerate.AtendimentoHttpEnum;
import com.conexa.atendimento.lib.resource.AtendimentoResource;

@RestController
@RequestMapping("/atendimento-agendamento")
public class AgendamentoConsultaResource implements AtendimentoResource {
	
	@Autowired
	private AgendamentoService agendamentoService;
	
	@GetMapping("/agendamento")
	public ResponseEntity<?> listarAgendamentos(@RequestHeader("idMedico") Long idMedico) {
		try {
			List<AgendamentoDto> response = agendamentoService.listarAgendamentos(idMedico);
			return retornarSucesso(response);
		} catch (Exception e) {
			return retornarResponse(AtendimentoHttpEnum.HTTP_400, e.getMessage());
		}
	}
}
