# Desafio Técnico backend Conexa

## Como rodar?

Não houve tempo hábil para subir uma esteira com o docker-compose (apesar do arquivo ter sido criado) =/

Dessa forma, foi testado somente usando NetBeans:

1. Suba o container mysql/adminer do docker-compose: `docker-compose up`
2. Abra o workspace inteiro, importando os projetos `atendimento-agendamento-backend`, `atendimento-paciente-backend`, `atendimento-lib`, `atendimento-gateway` no Eclipse STS
3. Suba todos os projetos pelo eclipse
4. Para testar, é só importar no postman a collection e as variáveis de ambiente (selecione a variável de ambiente CONEXA - LOCAL)
5. Médicos de exemplo `usuario / senha`: `wagner / abc123` e `andreia / abc123`

Qualquer dúvida, só me perguntar :)
