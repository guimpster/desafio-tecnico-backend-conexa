package com.conexa.atendimento.paciente.service;

import java.util.List;

import com.conexa.atendimento.lib.dto.PacienteDto;

public interface PacienteService {

	public PacienteDto inserirPaciente(PacienteDto pacienteDto);

	public List<PacienteDto> listarPacientes(String cpf, Long id);

	public void deletarPaciente(Long id);

	public PacienteDto atualizarPaciente(PacienteDto pacienteDto);

}
