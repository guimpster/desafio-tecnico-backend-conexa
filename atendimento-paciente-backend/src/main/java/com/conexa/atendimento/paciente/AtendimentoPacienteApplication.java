package com.conexa.atendimento.paciente;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.conexa.atendimento.lib.repository.PacienteRepository;

@SpringBootApplication
@EntityScan({ "com.conexa.atendimento.lib.domain" })
@EnableJpaRepositories(basePackageClasses= {PacienteRepository.class})
public class AtendimentoPacienteApplication {

	public static void main(String[] args) {
		SpringApplication.run(AtendimentoPacienteApplication.class, args);
	}

}
