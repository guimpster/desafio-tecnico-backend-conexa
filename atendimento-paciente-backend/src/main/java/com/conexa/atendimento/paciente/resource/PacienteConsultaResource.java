package com.conexa.atendimento.paciente.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.conexa.atendimento.lib.dto.PacienteDto;
import com.conexa.atendimento.lib.enumerate.AtendimentoHttpEnum;
import com.conexa.atendimento.lib.resource.AtendimentoResource;
import com.conexa.atendimento.paciente.service.PacienteService;

@RestController
@RequestMapping("/atendimento-paciente")
public class PacienteConsultaResource implements AtendimentoResource {
	
	@Autowired
	private PacienteService pacienteService;
	
	@GetMapping("/paciente")
	public ResponseEntity<?> consultarPacientes(
			@RequestParam(required = false) String cpf,
			@RequestParam(required = false) Long id) {
		try {
			List<PacienteDto> response = pacienteService.listarPacientes(cpf, id);
			return response != null && !response.isEmpty() ? retornarSucesso(response): retornarSemConteudo();
		} catch (Exception e) {
			return retornarResponse(AtendimentoHttpEnum.HTTP_400, e.getMessage());
		}
	}
}
