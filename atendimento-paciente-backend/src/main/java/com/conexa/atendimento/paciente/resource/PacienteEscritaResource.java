package com.conexa.atendimento.paciente.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.conexa.atendimento.lib.dto.PacienteDto;
import com.conexa.atendimento.lib.enumerate.AtendimentoHttpEnum;
import com.conexa.atendimento.lib.resource.AtendimentoResource;
import com.conexa.atendimento.paciente.service.PacienteService;

@RestController
@RequestMapping("/atendimento-paciente")
public class PacienteEscritaResource implements AtendimentoResource {
	
	@Autowired
	private PacienteService pacienteService;
	
	@PostMapping("/paciente")
	public ResponseEntity<?> inserirPaciente(@RequestBody(required = true) PacienteDto paciente) {	
		try {
			PacienteDto retorno = pacienteService.inserirPaciente(paciente);
			return retornarSucesso(retorno);
		} catch (Exception e) {
			return retornarResponse(AtendimentoHttpEnum.HTTP_400, e.getMessage());
		}
	}
	
	@DeleteMapping("/paciente")
	public ResponseEntity<?> deletarPaciente(@RequestParam(required = true) Long id) {	
		try {
			pacienteService.deletarPaciente(id);
			return retornarSucesso();
		} catch (Exception e) {
			return retornarResponse(AtendimentoHttpEnum.HTTP_400, e.getMessage());
		}
	}
	
	@PutMapping("/paciente")
	public ResponseEntity<?> atualizarPaciente(@RequestBody(required = true) PacienteDto paciente) {	
		try {
			PacienteDto retorno = pacienteService.atualizarPaciente(paciente);
			return retornarSucesso(retorno);
		} catch (Exception e) {
			return retornarResponse(AtendimentoHttpEnum.HTTP_400, e.getMessage());
		}
	}
}
