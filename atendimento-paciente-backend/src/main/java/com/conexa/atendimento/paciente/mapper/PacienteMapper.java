package com.conexa.atendimento.paciente.mapper;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.conexa.atendimento.lib.domain.Paciente;
import com.conexa.atendimento.lib.dto.PacienteDto;

@Component
public class PacienteMapper {

	public Paciente montarPaciente(PacienteDto pacienteDto) {
		Paciente paciente = new Paciente();
		paciente.setCpf(pacienteDto.getCpf());
		paciente.setIdade(pacienteDto.getIdade());
		paciente.setNome(pacienteDto.getNome());
		paciente.setTelefone(pacienteDto.getTelefone());
		paciente.setIdPaciente(pacienteDto.getId());
		return paciente;
	}
	
	public PacienteDto montarPacienteDto(Paciente paciente) {
		PacienteDto pacienteDto = new PacienteDto();
		pacienteDto.setCpf(paciente.getCpf());
		pacienteDto.setIdade(paciente.getIdade());
		pacienteDto.setNome(paciente.getNome());
		pacienteDto.setTelefone(paciente.getTelefone());
		pacienteDto.setId(paciente.getIdPaciente());
		return pacienteDto;
	}

	public List<PacienteDto> mapearPacientes(List<Paciente> pacientes) {
		if (pacientes == null) return Collections.emptyList();
		return pacientes.parallelStream().map(this::montarPacienteDto).collect(Collectors.toList());
	}

	public Paciente montarNovoPaciente(Paciente pacienteAntigo, PacienteDto pacienteDto) {
		Paciente paciente = new Paciente();
		paciente.setIdade(pacienteDto.getIdade() == null ? pacienteAntigo.getIdade() : pacienteDto.getIdade());
		paciente.setNome(pacienteDto.getNome() == null ? pacienteAntigo.getNome() : pacienteDto.getNome());
		paciente.setTelefone(pacienteDto.getTelefone() == null ? pacienteAntigo.getTelefone() : pacienteDto.getTelefone());
		
		paciente.setCpf(pacienteAntigo.getCpf());
		paciente.setIdPaciente(pacienteAntigo.getIdPaciente());
		return paciente;
	}
}
