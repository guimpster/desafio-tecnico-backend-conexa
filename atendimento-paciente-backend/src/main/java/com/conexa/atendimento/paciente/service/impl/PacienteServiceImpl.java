package com.conexa.atendimento.paciente.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.conexa.atendimento.lib.domain.Paciente;
import com.conexa.atendimento.lib.dto.PacienteDto;
import com.conexa.atendimento.lib.repository.PacienteRepository;
import com.conexa.atendimento.paciente.mapper.PacienteMapper;
import com.conexa.atendimento.paciente.service.PacienteService;

@Service
public class PacienteServiceImpl implements PacienteService {
	
	@Autowired
	private PacienteRepository pacienteRepository;
	
	@Autowired
	private PacienteMapper pacienteMapper;

	public PacienteDto inserirPaciente(PacienteDto pacienteDto) {
		// TODO Validações
		
		Paciente paciente = pacienteMapper.montarPaciente(pacienteDto);
		
		Paciente dbPaciente = pacienteRepository.save(paciente);
		
		return pacienteMapper.montarPacienteDto(dbPaciente);
	}

	@Override
	public List<PacienteDto> listarPacientes(String cpf, Long id) {
		List<Paciente> pacientes = new ArrayList<>();
		
		if (cpf != null) pacientes = pacienteRepository.findByCpf(cpf);
		else if (id != null) pacienteRepository.findById(id).ifPresent(pacientes::add);
		else pacienteRepository.findAll().forEach(pacientes::add);
			
		return pacienteMapper.mapearPacientes(pacientes);
	}

	@Override
	public void deletarPaciente(Long id) {
		pacienteRepository.deleteById(id);
	}

	@Override
	public PacienteDto atualizarPaciente(PacienteDto pacienteDto) {
		Paciente pacienteAntigo = pacienteRepository.findById(pacienteDto.getId())
				.orElseThrow(() -> new EntityNotFoundException("Paciente não encontrado"));
		
		Paciente pacienteNovo = pacienteRepository.save(pacienteMapper.montarNovoPaciente(pacienteAntigo, pacienteDto));
		
		return pacienteMapper.montarPacienteDto(pacienteNovo);
	}
}
