package com.conexa.atendimento.gateway.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.conexa.atendimento.gateway.service.LoginService;
import com.conexa.atendimento.lib.dto.LoginDto;
import com.conexa.atendimento.lib.dto.LoginMedicoDto;
import com.conexa.atendimento.lib.enumerate.AtendimentoHttpEnum;
import com.conexa.atendimento.lib.resource.AtendimentoResource;

@RestController
@RequestMapping("/api")
public class GatewayEscritaResource implements AtendimentoResource {
	
	@Autowired
	private LoginService loginService;
	
	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody(required = true) LoginDto loginDto) {	
		try {
			LoginMedicoDto retorno = loginService.login(loginDto.getUsuario(), loginDto.getSenha());
			return retornarSucesso(retorno);
		} catch (Exception e) {
			return retornarResponse(AtendimentoHttpEnum.HTTP_400, e.getMessage());
		}
	}
	
	@PostMapping("/logout")
	public ResponseEntity<?> logout(@RequestHeader("Authorization") String token) {	
		try {
			loginService.logout(token);
			return retornarSucesso();
		} catch (Exception e) {
			return retornarResponse(AtendimentoHttpEnum.HTTP_400, e.getMessage());
		}
	}
}
