package com.conexa.atendimento.gateway.security;

import java.util.Base64;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenProvider {
	
    private static final String JWT_CLAIM_MEDICO = "medico";
    private static final String AUTH_HEADER = "Authorization";
    
    // TODO: colocar em variaveis de ambiente
    private String secretKey="secret-key";
    // TODO: colocar em variaveis de ambiente
    private long validityInMilliseconds = 24*60*60*1000; // 1d

    @PostConstruct
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    public String createToken(String usuario, Long idMedico) {
        Claims claims = Jwts.claims().setSubject(usuario);
        claims.put(JWT_CLAIM_MEDICO, idMedico);

        Date now = new Date();
        Date validity = new Date(now.getTime() + validityInMilliseconds);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    public String resolveToken(HttpServletRequest req) {
        return req.getHeader(AUTH_HEADER);
    }

    public boolean validateToken(String token) {
        Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
        return true;
    }
    
    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }
    
    public Long getIdMedico(String token) {
        return Long.valueOf((Integer) Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().get(JWT_CLAIM_MEDICO));
    }
    
    public UserDetails getUserDetails(String token) {
        String userName = getUsername(token);
        Long idMedico = getIdMedico(token);
        String[] authorities = {};
        return new JwtUserDetails(userName, idMedico, authorities);
    }

	public Authentication getAuthentication(String token) {
		UserDetails userDetails = getUserDetails(token);
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
	}
}
