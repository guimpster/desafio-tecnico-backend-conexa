package com.conexa.atendimento.gateway.service.impl;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.conexa.atendimento.gateway.mapper.LoginMapper;
import com.conexa.atendimento.gateway.security.JwtTokenProvider;
import com.conexa.atendimento.gateway.service.LoginService;
import com.conexa.atendimento.lib.domain.Agendamento;
import com.conexa.atendimento.lib.domain.Medico;
import com.conexa.atendimento.lib.dto.LoginMedicoDto;
import com.conexa.atendimento.lib.repository.AgendamentoRepository;
import com.conexa.atendimento.lib.repository.MedicoRepository;

@Service
public class LoginServiceImpl implements LoginService {
	
	@Autowired
	JwtTokenProvider jwtTokenProvider;
	
	@Autowired
	MedicoRepository medicoRepository;
	
	@Autowired
	AgendamentoRepository agendamentoRepository;
	
	@Autowired
	LoginMapper loginMapper;

	@Override
	public LoginMedicoDto login(String usuario, String senha) {
		Medico medico = medicoRepository.findByUsuarioAndSenha(usuario, senha);
		if (medico == null) throw new EntityNotFoundException("Usuário não encontrado");
		String token = jwtTokenProvider.createToken(medico.getUsuario(), medico.getIdMedico());
		medico.setToken(token);
		medicoRepository.save(medico);
		List<Agendamento> agendamentos = agendamentoRepository.findByMedicoIdMedico(medico.getIdMedico());
		return loginMapper.montarLoginMedicoDto(medico, agendamentos);
	}

	@Override
	public void logout(String token) {
		Medico medico = medicoRepository.findByToken(token);
		if (medico == null) throw new EntityNotFoundException("Usuário não encontrado");
		medico.setToken(null);
		medicoRepository.save(medico);
	}
}
