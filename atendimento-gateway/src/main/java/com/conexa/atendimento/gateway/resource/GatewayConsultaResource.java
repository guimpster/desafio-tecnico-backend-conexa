package com.conexa.atendimento.gateway.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.conexa.atendimento.gateway.security.JwtTokenProvider;
import com.conexa.atendimento.lib.enumerate.AtendimentoHttpEnum;
import com.conexa.atendimento.lib.resource.AtendimentoResource;

@RestController
@RequestMapping("/api")
public class GatewayConsultaResource implements AtendimentoResource {
	
	@Autowired
	JwtTokenProvider jwtTokenProvider;
	
	@GetMapping("/valid/token")
	public ResponseEntity<?> validarToken(@RequestHeader("Authorization") String token) {
		try {
			jwtTokenProvider.validateToken(token);
			return retornarSucesso();
		} catch (Exception e) {
			return retornarResponse(AtendimentoHttpEnum.HTTP_400, e.getMessage());
		}
	}
}
