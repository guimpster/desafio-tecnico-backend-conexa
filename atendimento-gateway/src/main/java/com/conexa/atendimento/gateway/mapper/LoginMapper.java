package com.conexa.atendimento.gateway.mapper;

import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.conexa.atendimento.lib.constants.AtendimentoConstants;
import com.conexa.atendimento.lib.domain.Agendamento;
import com.conexa.atendimento.lib.domain.Medico;
import com.conexa.atendimento.lib.dto.AgendamentoDto;
import com.conexa.atendimento.lib.dto.LoginMedicoDto;

@Component
public class LoginMapper {
	public LoginMedicoDto montarLoginMedicoDto(Medico medico, List<Agendamento> agendamentos) {
		LoginMedicoDto loginMedicoDto = new LoginMedicoDto();
		loginMedicoDto.setToken(medico.getToken());
		loginMedicoDto.setMedico(medico.getMedico());
		loginMedicoDto.setEspecialidade(medico.getEspecialidade());
		loginMedicoDto.setAgendamentoHoje(montarAgendmentos(agendamentos));
		return loginMedicoDto;
	}

	private List<AgendamentoDto> montarAgendmentos(List<Agendamento> agendamentos) {
		if (agendamentos == null || agendamentos.isEmpty()) return Collections.emptyList();
		return agendamentos.parallelStream().map(this::montarAgendamento).collect(Collectors.toList());
	}
	
	private AgendamentoDto montarAgendamento(Agendamento agendamento) {
		AgendamentoDto agendamentoDto = new AgendamentoDto();
		agendamentoDto.setDataHoraAtendimento(agendamento.getDataHoraAtendimento().format(DateTimeFormatter.ofPattern(AtendimentoConstants.DATE_FORMAT)));
		agendamentoDto.setIdPaciente(agendamento.getPaciente().getIdPaciente());
		return agendamentoDto;
	}
}
