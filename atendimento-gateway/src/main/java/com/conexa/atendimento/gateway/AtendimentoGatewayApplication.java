package com.conexa.atendimento.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.conexa.atendimento.lib.repository.AgendamentoRepository;
import com.conexa.atendimento.lib.repository.MedicoRepository;

@EnableZuulProxy
@SpringBootApplication
@EntityScan({ "com.conexa.atendimento.lib.domain" })
@EnableJpaRepositories(basePackageClasses = { MedicoRepository.class, AgendamentoRepository.class })
public class AtendimentoGatewayApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(AtendimentoGatewayApplication.class, args);
	}

}
