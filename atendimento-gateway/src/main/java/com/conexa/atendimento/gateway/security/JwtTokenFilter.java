package com.conexa.atendimento.gateway.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import com.conexa.atendimento.gateway.exception.AuthException;
import com.conexa.atendimento.lib.repository.MedicoRepository;
import com.conexa.atendimento.lib.resource.HeaderMapRequestWrapper;

import io.jsonwebtoken.JwtException;

@Component
public class JwtTokenFilter extends GenericFilterBean {

	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	
	@Autowired
	private MedicoRepository medicoRepository;

    public JwtTokenFilter(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        String token = jwtTokenProvider.resolveToken((HttpServletRequest) req);
        
        if (token == null) {
        	filterChain.doFilter(req, res);
        	return;
        }
        
        if (medicoRepository.findByToken(token) == null) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token em formato inválido!");
            throw new AuthException("Token inválido!", HttpStatus.UNAUTHORIZED);
        }
        
        try {
            jwtTokenProvider.validateToken(token);
        } catch (JwtException | IllegalArgumentException e) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token em formato inválido!");
            throw new AuthException("Token inválido!", HttpStatus.UNAUTHORIZED);
        }
        
        Authentication auth = jwtTokenProvider.getAuthentication(token);

        SecurityContextHolder.getContext().setAuthentication(auth);
        
        String idMedico = String.valueOf(((JwtUserDetails) auth.getPrincipal()).getIdMedico());
        
        HttpServletRequest request = (HttpServletRequest) req;
        HeaderMapRequestWrapper requestWrapper = new HeaderMapRequestWrapper(request);
        requestWrapper.addHeader("idMedico", idMedico);
        
        filterChain.doFilter(requestWrapper, res);
    }
}
