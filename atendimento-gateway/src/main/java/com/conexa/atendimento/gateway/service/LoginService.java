package com.conexa.atendimento.gateway.service;

import com.conexa.atendimento.lib.dto.LoginMedicoDto;


public interface LoginService {

	public LoginMedicoDto login(String usuario, String senha);

	public void logout(String token);

}
